import React from 'react';
import '../App.css';
import RegisterForm from '../Forms/RegisterForm'

function Register() {
  return (
    <div className="Register">
      <h1>Welcome!</h1>
      <h2>Register to SurveyPuppy</h2>
      < RegisterForm />
    </div>
  );
};

export default Register;
