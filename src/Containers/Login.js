import React from 'react';
import '../App.css';
import LoginForm from '../Forms/LoginForm'

function Login() {
    return (
        <div className="Login">
            <h1>Welcome!</h1>
            <h2>Login to SurveyPuppy</h2>
            < LoginForm />
        </div>
    );
};

export default Login;
