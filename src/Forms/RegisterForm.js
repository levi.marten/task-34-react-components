import React from 'react';


const RegisterForm = () => {

    return (
        <form>
            <div>
                <label hidden>Username </label>
                <input type="text" placeholder="Choose a username...">
                </input>
            </div>
            <div>
                <label hidden>Password </label>
                <input type="password" placeholder="Choose a password...">
                </input>
            </div>
            <div>
                <button>Sign up</button>
            </div>
        </form>
    )
}

export default RegisterForm;