import React from 'react';

const LoginForm = () => {

    return (
        <form>
            <div>
                <label hidden>Username </label>
                <input type="text" placeholder="Enter your username...">
                </input>
            </div>
            <div>
                <label hidden>Password </label>
                <input type="password" placeholder="Enter your password...">
                </input>
            </div>
            <div>
                <button>Login</button>
            </div>
        </form>
    )
}

export default LoginForm;