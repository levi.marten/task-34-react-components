import React from 'react';
import './App.css';
import Register from './Containers/Register';
import Login from './Containers/Login';
import Dashboard from './Containers/Dashboard';

function App() {
  return (
    <div className="App">
      < Dashboard />
    </div>
  );
}

export default App;
